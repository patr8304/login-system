<?php
    session_start();
    include_once "db_conn.php";

    if (isset($_SESSION['signedin']) && $_SESSION['email'] == true) {
        //Doesn't do anything, just skips down the the html part
    } else {
        echo '<script>';
            echo 'alert("Please sign in to see this page")';
        echo '</script>';

        echo "<script type='text/javascript'> document.location = './index.php'; </script>";
    }
?>


<!DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="UFT-8">
        <title>Home</title>

        <link rel="stylesheet" href="./style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        
    </head>
    
    <body>
        <div class="signin">
            <form action="./signout.php" method="POST">
                <button type="submit">Sign out</button>
            </form>
        </div>
    </body>
</HTML>