<?php
    session_start();
    $_SESSION["signedup"] = true;
    include_once "db_conn.php";
?>

<!DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="UFT-8">
        <title>Sign up</title>

        <link rel="stylesheet" href="./style.css">
    </head>
    
    <body>
        <body>
            <div class="signin">
                <form class="form-signin" autocomplete="off" action="./signup_succes.php" method="POST">
                    <h1>Sign up</h1>

                    <input type="email" placeholder="Email address" name="inputEmail" required autofocus>
                    <input type="password" placeholder="Password" name="inputPassword" required>
                    <input type="password" placeholder="Confirm Password" name="inputPassword_confirm" required>
    
                    <button type="submit">Sign up</button>
                    <p class="copyright">© 2019 by Patrick Frost Sørensen</p>
                </form>
            </div>
        </body>
    </body>
</HTML>