<?php
    include_once "db_conn.php";
?>

<!DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="UFT-8">
        <title>Sign in</title>

        <link rel="stylesheet" href="./style.css">
    </head>
    
    <body>
        <div class="signin">        
            <form autocomplete="off" action="./signin_succes.php" method="POST">
                <h1>Sign in</h1>

                <input type="email" placeholder="Email address" name="inputEmail" required autofocus>
                <input type="password" placeholder="Password" name="inputPassword" required>

                <button type="submit">Sign in</button>
            </form>
        </div>
        <div class="signup">
            <p>Haven't <a href="./signup.php">Signed up</a> yet?</p>
            <p class="copyright">© 2019 by Patrick Frost Sørensen</p>
        </div>
    </body>
</HTML>
